import React from 'react';
import { useRouter } from 'next/router';
import { Modal } from '@redq/reuse-modal';
import { withApollo } from 'helper/apollo';
import { SEO } from 'components/seo';
import StoreNav from 'components/StoreNav/StoreNav';
import Carousel from 'components/Carousel/Carousel';
import Banner from 'containers/Banner/Banner';
import Sidebar from 'containers/Sidebar/Sidebar';
import Products from 'containers/Products/Products';
import ProductsPoint from 'containers/Products/ProductsPoint';
import CartPopUp from 'containers/Cart/CartPopUp';
import GiftCardPage from '../containers/CenterView/GiftCardPage';

import TitleBar from 'components/TitleBar/TitleBar';
import {
  MainContentArea,
  SidebarSection,
  ContentSection,
  OfferSection,
  MobileCarouselDropdown,
} from 'styled/pages.style';
// Static Data Import Here
import OFFERS from 'data/offers';
import BannerImg from 'image/grocery.png';
import storeType from 'constants/storeType';

const PAGE_TYPE = 'grocery';

function HomePage({ deviceType }) {
  const { query } = useRouter();
  const targetRef = React.useRef(null);
  React.useEffect(() => {
    if ((query.text || query.category) && targetRef.current) {
      window.scrollTo({
        top: targetRef.current.offsetTop - 110,
        behavior: 'smooth',
      });
      
    }
  }, [query]);

  return (
    <>
      <SEO title='東京みらい市2020' description='東京みらい市2020公式ホームページ' />
      <Modal>
        <Banner
          intlTitleId='groceriesTitle'
          intlDescriptionId='groceriesSubTitle'
          intlDescriptionPlace='groceriesSubTitlePlace'
          imageUrl={BannerImg}
        />

        {deviceType.desktop ? (
          <>
            <OfferSection>
              <div style={{ margin: '0 -10px' }}>
                <Carousel deviceType={deviceType} data={OFFERS} />
              </div>
            </OfferSection>

            <GiftCardPage deviceType={deviceType}/>

            {/* Title bar */}
            <TitleBar title='BBB販売店様の商品'/>

            <MobileCarouselDropdown>
              <Sidebar type={PAGE_TYPE} deviceType={deviceType} />
            </MobileCarouselDropdown>

            <MainContentArea>
              <SidebarSection>
                <Sidebar type={PAGE_TYPE} deviceType={deviceType} />
              </SidebarSection>
              <ContentSection>
                <div ref={targetRef}>
                  <Products
                    type={PAGE_TYPE}
                    deviceType={deviceType}
                    fetchLimit={8}
                  />
                </div>
              </ContentSection>
            </MainContentArea>
          </>
        ) : (
          <MainContentArea>
            <OfferSection>
              <div style={{ margin: '0 -10px' }}>
                <Carousel deviceType={deviceType} data={OFFERS} />
              </div>
            </OfferSection>

            {/* Title bar */}
            <TitleBar title='BBB販売店様の商品'/>
            
            <Sidebar type={PAGE_TYPE} deviceType={deviceType} />
            
            <ContentSection style={{ width: '100%' }}>
              <Products
                type={PAGE_TYPE}
                deviceType={deviceType}
                fetchLimit={8}
              />
            </ContentSection>
          </MainContentArea>
        )}

        {/* Title bar */}
        <TitleBar title='ポイント交換対象商品'/>
        {/* List of Products be changed by points */}
        <MainContentArea>
            <ContentSection style={{ width: '100%' }}>
              <ProductsPoint
                type={PAGE_TYPE}
                deviceType={deviceType}
                fetchLimit={8}
              />
            </ContentSection>
          </MainContentArea>

        <CartPopUp deviceType={deviceType} />
      </Modal>
    </>
  );
}

export default withApollo(HomePage);
