import styled, { keyframes } from 'styled-components';
import { themeGet } from '@styled-system/theme-get';

const TitleBarWrapper = styled.div`
  width: '100%';
  z-index : 10;
  height: 46px;
  padding: 12px 10px;
  flex-direction: row;
  align-items: center;
  background-color: ${themeGet('colors.black', '#0D1136')};

  @media (max-width: 767px) {
    width: 100%;
    height: 36px;
    padding: 8px 8px;
  }
`;
export default TitleBarWrapper;

export const LeftView = styled.span`
  width : 40px;
  height: '100%';
  padding: 0 10px;
  background-color: ${themeGet('colors.darkRed', '#f7f7f7')};
  border: solid 3px ${themeGet('colors.white', '#f7f7f7')};
  white-space: nowrap;

  @media (max-width: 767px) {
    padding: 0 8px;
  }
`;

export const TitleView = styled.span`
  height: '100%';
  white-space: nowrap;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  padding-left : 20px;
  color: ${themeGet('colors.white', '#0D1136')};

  @media (max-width: 767px) {
    font-size: 14px;
    padding-left : 14px;
  }
`;
