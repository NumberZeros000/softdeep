import React from 'react';
import Router, { useRouter } from 'next/router';
import { openModal } from '@redq/reuse-modal';
import SearchBox from 'components/SearchBox/SearchBox';
import { SearchContext } from 'contexts/search/search.context';
import { AuthContext } from 'contexts/auth/auth.context';
import TitleBarWrapper, {
  LeftView, TitleView
} from './TitleBar.style';

import LogoImage from 'image/logo.png';
import UserImage from 'image/user.jpg';

type Props = {
  className?: string;
  title?: string;
};

const TitleBar: React.FC<Props> = ({ className, title}) => {
  return (
    <TitleBarWrapper className={''}>
      <LeftView></LeftView>
      <TitleView>{title}</TitleView>
    </TitleBarWrapper>
  );
};

export default TitleBar;
