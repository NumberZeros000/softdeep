import React from 'react';
import Image from 'components/Image/Image';
import {
  ProductName,
  PointView,
  ProductCardWrapper,
  ProductImageWrapper,
  ProductPointInfo,
  ProductPointName
} from './ProductCard.style';

type ProductCardProps = {
  title: string;
  image: any;
  discountInPercent?: number;
  onClick?: (e: any) => void;
};

const ProductCard: React.FC<ProductCardProps> = ({
  title,
  image,
  discountInPercent,
  onClick,
}) => {
  return (
    <ProductCardWrapper onClick={onClick} className='furniture-card'>
      <ProductImageWrapper>
        <Image
          url={image}
          className='product-image'
          style={{ position: 'relative' }}
          alt={title}
        />
        {discountInPercent ? (
          <>
            <PointView>{discountInPercent} pts</PointView>
          </>
        ) : (
          ''
        )}
      </ProductImageWrapper>
      
      <ProductPointInfo>
        <ProductPointName>{title}</ProductPointName>
      </ProductPointInfo>
    </ProductCardWrapper>
  );
};

export default ProductCard;
