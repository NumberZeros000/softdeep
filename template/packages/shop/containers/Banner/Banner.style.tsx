import styled from 'styled-components';
import { themeGet } from '@styled-system/theme-get';

const BannerWrapper = styled.div`
  width: 100%;
  min-height: 50vh;
  display: flex;
  align-items: center;
  overflow: hidden;
  background-size: 100%;
  background-position: bottom center;
  background-repeat: no-repeat;
  background-color: #131d27;
  background-size: cover;

  @media (max-width: 1400px) {
    min-height: 50vh;
  }

  @media (max-width: 1200px) {
    min-height: 38vh;
  }

  @media (max-width: 1050px) {
    min-height: 25vh;
  }

  @media (max-width: 990px) {
    min-height: 260px;
    padding-top: 50px;
  }

  @media (max-width: 767px) {
    min-height: 180px;
    padding-top: 45px;
  }
`;

const BannerComponent = styled('div')`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;

  .banner-search {
    @media (max-width: 990px) {
      display: none;
    }
  }
`;

const BannerHeading = styled('h1')`
  font-family: ${themeGet('fontFamily.1', 'sans-serif')};
  font-size: ${themeGet('fontSizes.7', '50')}px;
  font-weight: 700;
  color: #fff;
  margin-bottom: 5px;
  text-align: center;

  @media (max-width: 1400px) {
    font-size: ${themeGet('fontSizes.6', '45')}px;
  }
  @media (max-width: 990px) {
    font-size: ${themeGet('fontSizes.5', '34')}px;
  }
  @media (max-width: 767px) {
    font-size: 26px;
    padding-top: 10px;
    margin-bottom: 0px;
  }
`;

const BannerSubHeading = styled('span')`
  font-family: ${themeGet('fontFamily.0', 'sans-serif')};
  font-size: ${themeGet('fontSizes.3', '19')}px;
  font-weight: 400;
  color: #ccc;
  margin-bottom: 10px;
  text-align: center;

  @media (max-width: 1400px) {
    font-size: ${themeGet('fontSizes.2', '15')}px;
  }
  @media (max-width: 990px) {
    font-size: ${themeGet('fontSizes.1', '12')}px;
  }
  @media (max-width: 767px) {
    font-size: 12px;
    margin-bottom: 0px;
  }
`;

const BannerSubHeadingPlace = styled('span')`
  font-family: ${themeGet('fontFamily.0', 'sans-serif')};
  font-size: ${themeGet('fontSizes.3', '19')}px;
  font-weight: 400;
  color: #ccc;
  margin-bottom: 40px;
  text-align: center;

  @media (max-width: 1400px) {
    font-size: ${themeGet('fontSizes.2', '15')}px;
  }
  @media (max-width: 990px) {
    font-size: ${themeGet('fontSizes.1', '12')}px;
    margin-bottom: 10px;
  }
  @media (max-width: 767px) {
    font-size: 12px;
    margin-bottom: 0px;
  }
`;

export { BannerWrapper, BannerHeading, BannerSubHeading, BannerSubHeadingPlace, BannerComponent };
