import React from 'react';
import NavLink from 'components/NavLink/NavLink';
import Button from 'components/Button/Button';
import Popover from 'components/Popover/Popover';
import { OFFER_PAGE, HELP_PAGE } from 'constants/navigation';
import { AuthorizedMenu } from '../AuthorizedMenu';
import LanguageSwitcher from '../LanguageSwitcher/LanguageSwitcher';
import { HelpIcon } from 'components/AllSvgIcon';
import { RightMenuBox } from './RightMenu.style';

type Props = {
  onLogout: () => void;
  onJoin: () => void;
  avatar: string;
  isAuthenticated: boolean;
};

export const RightMenu: React.FC<Props> = ({
  onLogout,
  avatar,
  isAuthenticated,
  onJoin
}) => {
  return (
    <RightMenuBox>
      <NavLink
        className="how-item"
        href={'/'}
        label="使い方"
        iconClass="menu-icon"
        icon={<HelpIcon />}
      />

      {!isAuthenticated ? (
        <Button
          className="button-login"
          onClick={onJoin}
          size="small"
          title="ログイン"
          style={{color: '#fff'}}
        />
      ) : (
        <Popover
          direction="right"
          className="user-pages-dropdown"
          handler={<img src={avatar} alt="user" />}
          content={<AuthorizedMenu onLogout={onLogout} />}
        />
      )}
    </RightMenuBox>
  );
};
