import React from 'react';
import { FormattedMessage } from 'react-intl';
import NavLink from 'components/NavLink/NavLink';

import {
  PROCEED_TO_CHECKOUT_PAGE,
  ALTERNATIVE_CHECKOUT_PAGE,
  PROFILE_PAGE,
  ORDER_RECEIVED,
  YOUR_ORDER,
} from 'constants/navigation';

const AUTHORIZED_MENU_ITEMS = [
  {
    link: '/',
    label: 'アカウント情報',
  },
  {
    link: '/',
    label: '注文履歴',
  },
  {
    link: '/',
    label: 'ポイント交換履歴',
  }
];

type Props = {
  onLogout: () => void;
};

export const AuthorizedMenu: React.FC<Props> = ({ onLogout }) => {
  return (
    <>
      {AUTHORIZED_MENU_ITEMS.map((item, idx) => (
        <NavLink
          key={idx}
          className='menu-item'
          href={item.link}
          label={item.label}
          intlId={''}
        />
      ))}
      <div className='menu-item' onClick={onLogout}>
        <a>
          <span>
            <FormattedMessage id='navlinkLogout' defaultMessage='Logout' />
          </span>
        </a>
      </div>
    </>
  );
};
