import React from 'react';
import { NextPage } from 'next';
import TitleBar from 'components/TitleBar/TitleBar';
import { Modal } from '@redq/reuse-modal';

import {
  LeftBoxWrapper,
  CenterBoxWrapper,
  RightBoxWrapper,
  PaddingWrapper
} from './GiftCardPage.style';

import GiftCard from 'components/GiftCard/GiftCard';

type GiftCardProps = {
  deviceType: {
    mobile: boolean;
    tablet: boolean;
    desktop: boolean;
  };
};

const GiftCardPage: NextPage<GiftCardProps> = ({ deviceType }) => {
  const coupons = [
    {id : '1', image : 'https://skyace.co.jp/tokyo/assets/point_1.png', code : '123'},
    {id : '1', image : 'https://skyace.co.jp/tokyo/assets/point_1.png', code : '123'},
    {id : '1', image : 'https://skyace.co.jp/tokyo/assets/point_1.png', code : '123'},
    {id : '1', image : 'https://skyace.co.jp/tokyo/assets/point_1.png', code : '123'}
  ];

  return (
    <Modal>
      {deviceType.desktop &&
      <div style={{ width: '100%', flexDirection : 'row', display: 'flex', paddingBottom: 30, paddingTop: 50}}>
        <LeftBoxWrapper>
          <TitleBar title='お買い得商品'/>
        </LeftBoxWrapper>
        <PaddingWrapper/>
        <CenterBoxWrapper>
          <div>
            <iframe width="550" height="275"
              src="https://www.youtube.com/embed/ooFjsL8r-uQ"
            ></iframe>
          </div>
        </CenterBoxWrapper>
        <PaddingWrapper/>
        <RightBoxWrapper/>
      </div>
      }

      {!deviceType.desktop &&
      <div style={{ width: '100%', flexDirection : 'row', display: 'flex', paddingBottom: 30}}>
        <CenterBoxWrapper>
          <div>
            <iframe width="550" height="275"
              src="https://www.youtube.com/embed/ooFjsL8r-uQ"
            ></iframe>
          </div>
        </CenterBoxWrapper>
        
        <LeftBoxWrapper>
          <TitleBar title='お買い得商品'/>
        </LeftBoxWrapper>
        <PaddingWrapper/>
        
        <PaddingWrapper/>
        <RightBoxWrapper/>
      </div>
      }

      
    </Modal>
    
  );
};
export default GiftCardPage;
