import styled from 'styled-components';
import { themeGet } from '@styled-system/theme-get';

export const LeftBoxWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 25%;
  height : 600px;
  background-color: ${themeGet('colors.hatBlue', '#77798C')};
  
  @media (max-width: 767px) {
    width: 60%;
  }

  @media (max-width: 630px) {
    width: 100%;
  }

  .couponCodeText {
    margin-right: auto;
  }
`;

export const RightBoxWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 25%;
  height : 600px;

  background-color: ${themeGet('colors.hatBlue', '#77798C')};

  @media (max-width: 767px) {
    width: 60%;
  }

  @media (max-width: 630px) {
    width: 100%;
  }

  .couponCodeText {
    margin-right: auto;
  }
`;

export const PaddingWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 30px;
  height : 100%;

  background-color: ${themeGet('colors.hatBlue', '#77798C')};

  @media (max-width: 767px) {
    width: 60%;
  }

  @media (max-width: 630px) {
    width: 100%;
  }

  .couponCodeText {
    margin-right: auto;
  }
`;

export const CenterBoxWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: calc(50% - 60px);
  height : 600px;

  div {
    position: relative;
    width: 100%;
    height: 0;
    padding-bottom: 60%;
  }

  iframe {
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
  }

  @media (max-width: 767px) {
    width: 60%;
  }

  @media (max-width: 630px) {
    width: 100%;
  }

  .couponCodeText {
    margin-right: auto;
  }
`;

