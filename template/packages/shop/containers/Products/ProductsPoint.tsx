import React, { useState } from 'react';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import gql from 'graphql-tag';
import { openModal, closeModal } from '@redq/reuse-modal';
import ProductCard from 'components/ProductCard/ProductCardFurniture';
import {
  ProductsRow,
  ProductsCol,
  ButtonWrapper,
  LoaderWrapper,
  LoaderItem,
  ProductCardWrapper,
} from './Products.style';
import { CURRENCY } from 'helper/constant';
import { useQuery } from '@apollo/react-hooks';
import Button from 'components/Button/Button';
import Loader from 'components/Loader/Loader';
import Placeholder from 'components/Placeholder/Placeholder';
import Fade from 'react-reveal/Fade';
import NoResultFound from 'components/NoResult/NoResult';
import { timeout } from 'q';

const QuickPointView = dynamic(() => import('../QuickView/QuickPointView'));

const GET_PRODUCTS = gql`
  query getProducts(
    $type: String
    $text: String
    $category: String
    $offset: Int
    $limit: Int
  ) {
    products(
      type: $type
      text: $text
      category: $category
      offset: $offset
      limit: $limit
    ) {
      items {
        id
        title
        slug
        unit
        price
        salePrice
        description
        discountInPercent
        type
        image
        gallery {
          url
        }
        categories {
          id
          title
          slug
        }
      }
      hasMore
    }
  }
`;

type ProductsProps = {
  deviceType?: {
    mobile: boolean;
    tablet: boolean;
    desktop: boolean;
  };
  type: string;
  fetchLimit?: number;
  loadMore?: boolean;
};
export const ProductsPoint: React.FC<ProductsProps> = ({
  deviceType,
  type,
  fetchLimit = 8,
  loadMore = true,
}) => {
  const router = useRouter();
  const [loadingMore, toggleLoading] = useState(false);
  const { data, error, loading, fetchMore } = useQuery(GET_PRODUCTS, {
    variables: {
      type: type,
      text: router.query.text,
      category: router.query.category,
      offset: 0,
      limit: fetchLimit,
    },
  });

  const arrTitle = ['ノートパソコン or PlayStation 4', 'Nintendo Switch', 
      'USJ ペアチケット or 黒毛和牛 1kg', 'グルメ特選カタログ 10,000円分',
      'ゲームソフト', 'ハーゲンダッツアイスクリームセット',
      'お米券', '文房具セット'
    ];

  const arrDesc = [
    'PS4のリモートプレイとは、PS4とPCやMac、PS Vitaをネットワーク接続することで、PS4の画面を各デバイス上で共有する機能のことです。リモートプレイを使えば、たとえばPS4に接続しているテレビが使えない状況や、外出先からなどでもPS4のゲームをこれらのデバイス上でプレイすることができます', 
    'さまざまなプレイスタイルで遊べて、Joy-Conをつかったゲームが購入してすぐに遊べる「Nintendo Switch」。小さく、軽く、持ち運びやすい、携帯専用の「Nintendo Switch Lite」。あなたのライフスタイルにあわせて、お選びいただくことができます。', 
    'カブリ、ウチモモとも呼ぶ。モモ肉の内側に位置する。筋が少なく、脂肪が少ない部位でアッサリとした味わいを楽しめる。脂肪が少ない分、赤身の肉本来の旨みを存分にお楽しみいただけます。',
    'その人気の「国産和牛」だけを集めた専門グルメカタログギフトが、この「選べる国産和牛カタログギフト」です。日本全国の黒毛和牛を銘柄ごと、部位ごとに試食を重ね厳選した、本当に美味しい上質な味の肉のみを掲載いたしました', 
    'ゲームソフトは、コンピュータゲームのソフトウェア（コンピュータ・プログラム）である。本来、ソフトウェアとは物理的な形を持たないものなのであるが、ゲーム業界では主としてロムカセットの時代に「物」として販売することが確立したため、その結果として中古流通が盛んになるなどといったこともあり、ゲーム業界的には、本来はハードウェアであるメディア自体も「ソフト」に含まれてしまっていることも以前は多かったが、ダウンロード販売なども増えた近年では変わってきている。',
    'バニラ２個、ストロベリー２個、グリーンティー２個、 クッキー＆クリーム２個、マカデミアナッツ２個、クリスプチップチョコレート２個', 
    '全米販（全国米穀販売事業共済協同組合）が昭和58年から発行しているお米の商品券です。贈り物として幅広くご利用いただける券です。（お祝い・お礼・仏事・企業活動…。様々なシーンでご利用いただけます。）',
    'お祝いに最適な文具セット♪ひかりが厳選したオススメの文具セットです。お子様の使いやすさと機能性　の高い文房具たちを女の子に喜ばれるピンク色の文房具をテーマに１４アイテム揃えました。', 
  ];

  // Quick View Modal
  const handleModalClose = () => {
    const href = `${router.pathname}`;
    const as = '/';
    router.push(href, as, { shallow: true });
    closeModal();
  };
  const handleQuickViewModal = React.useCallback(
    (modalProps: any, deviceType: any, onModalClose: any) => {
      if (router.pathname === '/point/[slug]') {
        const as = `/point/${modalProps.slug}`;
        router.push(router.pathname, as);
        return;
      }
      openModal({
        show: true,
        overlayClassName: 'quick-view-overlay',
        closeOnClickOutside: false,
        component: QuickPointView,
        componentProps: { modalProps, deviceType, onModalClose },
        closeComponent: 'div',
        config: {
          enableResizing: false,
          disableDragging: true,
          className: 'quick-view-modal',
          width: 900,
          y: 30,
          height: 'auto',
          transition: {
            mass: 1,
            tension: 0,
            friction: 0,
          },
        },
      });
      const href = `${router.pathname}?${modalProps.slug}`;
      const as = `/point/${modalProps.slug}`;
      router.push(href, as, { shallow: true });
    },
    []
  );

  if (loading) {
    return (
      <LoaderWrapper>
        <LoaderItem>
          <Placeholder />
        </LoaderItem>
        <LoaderItem>
          <Placeholder />
        </LoaderItem>
        <LoaderItem>
          <Placeholder />
        </LoaderItem>
      </LoaderWrapper>
    );
  }

  if (error) return <div>{error.message}</div>;
  if (!data || !data.products || data.products.items.length === 0) {
    return <NoResultFound />;
  }

  return (
    <>
      <ProductsRow>
        {data.products.items.map((item: any, index: number) => {
          const pts = Math.floor(Math.random() * 1000) + 100;
          if (index >= 8) return null;
          return  (
              <ProductsCol key={index}>
                <ProductCardWrapper>
                  <Fade
                    duration={800}
                    delay={index * 10}
                    style={{ height: '100%' }}
                  >
                    <ProductCard
                      title={arrTitle[index]}
                      image={`https://skyace.co.jp/tokyo/assets/point_${index + 1}.png`}
                      discountInPercent={pts}
                      onClick={() =>
                        handleQuickViewModal({
                          title : arrTitle[index],
                          slug : `${index}`,
                          description : arrDesc[index],
                          discountInPercent : pts,
                          type : item.type,
                          image : item.image,
                          gallery : [{url : `https://skyace.co.jp/tokyo/assets/point_${index + 1}.png`}, {url : `https://skyace.co.jp/tokyo/assets/point_${index + 1}.png`}, {url : `https://skyace.co.jp/tokyo/assets/point_${index + 1}.png`}],
                          id : `${index}`
            }, deviceType, handleModalClose)
                      }
                    />
                  </Fade>
                </ProductCardWrapper>
              </ProductsCol>
            )
        })}
      </ProductsRow>
    </>
  );
};
export default ProductsPoint;
