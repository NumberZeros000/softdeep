import React, { useState } from 'react';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import gql from 'graphql-tag';
import { openModal, closeModal } from '@redq/reuse-modal';
import ProductCard from 'components/ProductCard/ProductCard';
import {
  ProductsRow,
  ProductsCol,
  ButtonWrapper,
  LoaderWrapper,
  LoaderItem,
  ProductCardWrapper,
} from './Products.style';
import { CURRENCY } from 'helper/constant';
import { useQuery } from '@apollo/react-hooks';
import Button from 'components/Button/Button';
import Loader from 'components/Loader/Loader';
import Placeholder from 'components/Placeholder/Placeholder';
import Fade from 'react-reveal/Fade';
import NoResultFound from 'components/NoResult/NoResult';

const QuickView = dynamic(() => import('../QuickView/QuickView'));

const GET_PRODUCTS = gql`
  query getProducts(
    $type: String
    $text: String
    $category: String
    $offset: Int
    $limit: Int
  ) {
    products(
      type: $type
      text: $text
      category: $category
      offset: $offset
      limit: $limit
    ) {
      items {
        id
        title
        slug
        unit
        price
        salePrice
        description
        discountInPercent
        type
        image
        gallery {
          url
        }
        categories {
          id
          title
          slug
        }
      }
      hasMore
    }
  }
`;

type ProductsProps = {
  deviceType?: {
    mobile: boolean;
    tablet: boolean;
    desktop: boolean;
  };
  type: string;
  fetchLimit?: number;
  loadMore?: boolean;
};
export const Products: React.FC<ProductsProps> = ({
  deviceType,
  type,
  fetchLimit = 8,
  loadMore = true,
}) => {
  const router = useRouter();
  const [loadingMore, toggleLoading] = useState(false);
  const { data, error, loading, fetchMore } = useQuery(GET_PRODUCTS, {
    variables: {
      type: type,
      text: router.query.text,
      category: router.query.category,
      offset: 0,
      limit: fetchLimit,
    },
  });
  // Quick View Modal
  const handleModalClose = () => {
    const as = router.asPath;
    router.push(as, as, { shallow: true });
    closeModal();
  };

  const handleQuickViewModal = (
    modalProps: any,
    deviceType: any,
    onModalClose: any
  ) => {
    if (router.pathname === '/product/[slug]') {
      const as = `/product/${modalProps.slug}`;
      router.push(router.pathname, as);
      return;
    }
    openModal({
      show: true,
      overlayClassName: 'quick-view-overlay',
      closeOnClickOutside: false,
      component: QuickView,
      componentProps: { modalProps, deviceType, onModalClose },
      closeComponent: 'div',
      config: {
        enableResizing: false,
        disableDragging: true,
        className: 'quick-view-modal',
        width: 900,
        y: 30,
        height: 'auto',
        transition: {
          mass: 1,
          tension: 0,
          friction: 0,
        },
      },
    });
    const href = router.asPath;
    const as = `/product/${modalProps.slug}`;
    router.push(href, as, { shallow: true });
  };

  if (loading) {
    return (
      <LoaderWrapper>
        <LoaderItem>
          <Placeholder />
        </LoaderItem>
        <LoaderItem>
          <Placeholder />
        </LoaderItem>
        <LoaderItem>
          <Placeholder />
        </LoaderItem>
      </LoaderWrapper>
    );
  }

  if (error) return <div>{error.message}</div>;
  if (!data || !data.products || data.products.items.length === 0) {
    return <NoResultFound />;
  }
  const handleLoadMore = () => {
    toggleLoading(true);
    fetchMore({
      variables: {
        offset: Number(data.products.items.length),
        limit: fetchLimit,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        toggleLoading(false);
        if (!fetchMoreResult) {
          return prev;
        }
        return {
          products: {
            __typename: prev.products.__typename,
            items: [...prev.products.items, ...fetchMoreResult.products.items],
            hasMore: fetchMoreResult.products.hasMore,
          },
        };
      },
    });
  };

  const arrImgUrl = [
      'https://skyace.co.jp/tokyo/assets/toto_1.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_1.png',
      'https://skyace.co.jp/tokyo/assets/kucho_1.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_1.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_2.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_2.png',
      'https://skyace.co.jp/tokyo/assets/kucho_2.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_2.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_3.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_3.png',
      'https://skyace.co.jp/tokyo/assets/kucho_3.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_3.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_4.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_4.png',
      'https://skyace.co.jp/tokyo/assets/kucho_4.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_4.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_5.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_5.png',
      'https://skyace.co.jp/tokyo/assets/kucho_5.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_5.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_6.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_6.png',
      'https://skyace.co.jp/tokyo/assets/kucho_6.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_6.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_7.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_7.png',
      'https://skyace.co.jp/tokyo/assets/kucho_7.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_7.jpg',
      'https://skyace.co.jp/tokyo/assets/toto_8.jpg',
      'https://skyace.co.jp/tokyo/assets/kanzai_8.png',
      'https://skyace.co.jp/tokyo/assets/kucho_8.png',
      'https://skyace.co.jp/tokyo/assets/jusetsu_8.jpg'
    ];

  const arrTitle = [
      'ネオレストNX', 
      'セキスイ VPWパイプ 水道管 4000L', 
      'ルームエアコン(うるさらX)【ホワイト】', 
      'ガス給湯器(本体＋インターホンリモコンセット)【24号】',
      'キッチン用水栓金具(ニューウェーブ)', 
      'セキスイ エスロン クリーンパイプ 4000L【13サイズ】',
      '空気清浄機(うるるとさらら)【ビターブラウン】',
      'ビルトインガスコンロ(デリシア,DELICIA)', 
    ];

  const arrDesc = [
    'TOTOネオレストシリーズの最上位グレード、ネオレストNXの一番の魅力は何といっても、陶器の質感を生かした優美なデザインです。さらに汚れにくさ・掃除のしやすさ・除菌機能などの最新機能も充実。トイレ空間にこだわりがある方や、おしゃれな店舗におすすめしたいタンクレストイレです。\n※ネオレストNXは、排水タイプ床200mmのみになります。\n※リモコンはスティックタイプのみになります。', 
    '排水、通気用エスロンパイプＶＰ・ＶＵは耐食性に優れたポリ塩化ビニル製。管内面が滑らかで摩擦抵抗が小さく、スライムが付着しにくいため、長年にわたり安定した流量を保持します。肉厚のＶＰ管はあらゆる排水設備に、薄肉のＶＵ管は一般の戸建住宅や簡易な排水設備に最適です。', 
    '現在、「壁掛設置タイプ」の給湯暖房熱源機をお使いの場合は、エコジョーズへの交換をおすすめします。壁掛設置タイプについては、非エコジョーズとエコジョーズの機器本体価格にあまり差がないため、ガス代がお安くなるエコジョーズに変えた方が、10年使用した際のトータルコストが安くなる場合があります。',
    '無給水加湿「うるる加湿」と熱交換器の汚れを洗浄する「加湿水洗浄」を搭載した、快適フラッグシップモデル「うるさらX」RXシリーズの2020年モデル。「AI快適自動運転」により湿度と温度をコントロールする。暖房は床面から温もりが広がる「垂直気流」により風を直接感じにくく、足元から温かい。冷房時は、天井に沿って気流を回し部屋全体を快適にする「サーキュレーション気流」により、温度と湿度を素早く下げます。', 
    
    '吐水口先端で止めたり出したりが可能なタッチスイッチ水栓です。スパウトの先端の切替で簡単に吐水の切替が可能です。',
    '重金属の溶出量が極めて少ないクリーンな材料を使用しいます。管内面の平滑性が高く、生菌増殖の原因となる凹凸、ボイドが極めて僅かです。特にTOC溶出性に優れています。', 
    '100V電源タイプのリンナイ最上位機種デザインが際立つリンナイの最高機種。Wワイド火力バーナーや、オートグリルなどの基本の機能はもちろん、 天面液晶表示で、操作内容が分かりやすいのが特長です。また、新たにコンロオートメニュー機能が搭載され、コツのいる焼き餃子やハンバーグなど5メニューを自動調理することが可能です。',
    '除湿・加湿・集じん・脱臭を1台で行う空気清浄機。室内の温度・湿度を診断し最適な運転モードを選択する「おまかせ運転」を搭載。無線LANを内蔵し、専用アプリ「Daikin Smart APP」に接続すれば、エアコンとの連動運転ができ、エアコン立ち上げ時の除湿と加湿をアシストする。「ツインストリーマ」を搭載し、有害物質の分解力と脱臭力が従来比約2倍に向上。「TAFUフィルター」で、10年後でも従来比約1.4倍の集じん能力を維持します。', 
  ];
  const arrPrice = [
    640000, 590, 495000, 550000, 90100, 844, 100000, 342000
  ];

  return (
    <>
      <ProductsRow>
        {data.products.items.map((item: any, index: number) => {
          const imgUrl = arrImgUrl[index % 16];
          const newItem = {
            title : arrTitle[index % 8],
            slug : `${index}`,
            description : arrDesc[index % 8],
            type : item.type,
            price: arrPrice[index % 8],
            image : imgUrl,
            gallery : [{url : imgUrl}, {url : imgUrl}, {url : imgUrl}],
            id : `${index}`
          }
          return (
          <ProductsCol key={index}>
            <ProductCardWrapper>
              <Fade
                duration={800}
                delay={index * 10}
                style={{ height: '100%' }}
              >
                <ProductCard
                  title={newItem.title}
                  description={newItem.description}
                  image={imgUrl}
                  weight={item.unit}
                  currency={CURRENCY}
                  price={newItem.price.toLocaleString()}
                  data={newItem}
                  deviceType={deviceType}
                  onClick={() =>
                    handleQuickViewModal(newItem, deviceType, handleModalClose)
                  }
                />
              </Fade>
            </ProductCardWrapper>
            </ProductsCol>
          )
        })}
      </ProductsRow>
      {loadMore && data.products.hasMore && (
        <ButtonWrapper>
          <Button
            onClick={handleLoadMore}
            title="Load More"
            intlButtonId="loadMoreBtn"
            size="small"
            isLoading={loadingMore}
            loader={<Loader color="#009E7F" />}
            style={{
              minWidth: 135,
              backgroundColor: '#ffffff',
              border: '1px solid #f1f1f1',
              color: '#009E7F',
            }}
          />
        </ButtonWrapper>
      )}
    </>
  );
};
export default Products;
