import axios from "axios";
import { get } from "./controls";

export const request = (endpoint, method, data) => {
  return new Promise(async (resolve, reject) => {
    const accessToken = await get("accessToken");
    const options = {
      method,
      url: endpoint,
      headers: {
        "Content-Type": "application/json",
        authencation: accessToken,
      },
      data: method !== "GET" ? data : null,
      params: method === "GET" ? data : null,
    };
    return axios(options)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        const { response } = error;
        reject({
          message: error.response?.data.message,
          status: error.response?.status,
        });
      });
  });
};

export const submitImage = (endpoint, data) => {
  return new Promise(async (resolve, reject) => {
    const options = {
      method: "post",
      url: endpoint,
      data: data,
      headers: { "Content-Type": "multipart/form-data" },
    };
    axios(options)
      .then(function (response) {
        //handle success
        resolve(response.data);
      })
      .catch(function (err) {
        //handle error
        reject({
          message: err.response?.data.message,
          status: err.response?.status,
        });
      });
  });
};

export const getLinkFromToken = (endpoint, method, data) => {
  return new Promise(async (resolve, reject) => {
    const options = {
      method,
      url: endpoint,
      headers: {
        "Content-Type": "application/json",
        authencation: data,
      },
    };
    axios(options)
      .then(function (response) {
        //handle success
        resolve(response.data);
      })
      .catch(function (err) {
        //handle error
        const { response } = err;
        reject({
          message: err.response?.data.message,
          status: err.response?.status,
        });
      });
  });
};

export default request;
