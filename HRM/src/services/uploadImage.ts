import { isEmpty } from "lodash";
import { apiBaseURL } from "../config";

import { request, submitImage } from "./api";
import moment from "moment";
import { message } from "antd";

export const UploadImage = {
  upLoadImage: async (file: any) => {
    try {
      if (file.target.files.length < 0)
        return alert("Please, choose file image");
      const data = file.target.files[0];
      const typeFile = ["image/jpeg", "image/png"];
      if (!typeFile.includes(data.type)) {
        // return message.error("You can only upload JPG/PNG file!");
      }
      const size = file.size / 1024;
      if (size > 2) {
        // return message.error("Image must smaller than 2MB!");
      }
      const time = moment().format("X");
      const name = `${time}${data.name.trim().replace(/\s/g, "")}`;
      const formData = new FormData();
      formData.append("image", data);
      formData.append("id", name);
      const res = await submitImage(`${apiBaseURL}/files/upload`, formData);
      if (isEmpty(res)) throw "Upload image fail";
      return res;
    } catch (err) {}
  },
};

export const UploadImageWithName = {
  upLoadImage: async (file: any, name) => {
    try {
      if (file.target.files.length < 0)
        return alert("Please, choose file image");
      const data = file.target.files[0];
      const typeFile = ["image/jpeg", "image/png"];
      if (!typeFile.includes(data.type)) {
        return message.error("You can only upload JPG/PNG file!");
      }
      const size = file.size / 1024;
      if (size > 2) {
        return message.error("Image must smaller than 2MB!");
      }
      const formData = new FormData();
      formData.append("image", data);
      formData.append("id", name);
      const res = await submitImage(`${apiBaseURL}/files/upload`, formData);
      if (isEmpty(res)) throw "Upload image fail";
      return res;
    } catch (err) {
      console.log("err", err);
    }
  },
};

export const UploadImageCertification = {
  upLoadImage: async (file: any, name: string) => {
    try {
      const data = file;
      const formData = new FormData();
      formData.append("image", data);
      formData.append("id", name);
      const res = await submitImage(`${apiBaseURL}/files/upload`, formData);
      if (isEmpty(res)) throw "Upload image fail";
      return res;
    } catch (err) {}
  },
};
