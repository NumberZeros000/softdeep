import { request } from "../services/api";
import { apiBaseURL } from "../config";

export function Login(data) {
  console.log(data);
  return request(`${apiBaseURL}/auth/login`, "post", data);
}
