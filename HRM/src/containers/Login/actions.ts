import { createAction } from "redux-actions";

export const handleLogin = createAction("HANDLE_LOGIN/LOGIN");
export const handleLoginSuccess = createAction("HANDLE_LOGIN/LOGIN_SUCCESS");
export const handleLoginFail = createAction("HANDLE_LOGIN/LOGIN_FAIL");
