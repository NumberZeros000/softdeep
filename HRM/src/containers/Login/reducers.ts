/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Login";

const initialState = freeze({
  data: null,
  userInfo: null,
  isAuthenticated: false,
});

export default handleActions(
  {
    [actions.handleLoginSuccess]: (state, action) => {
      return freeze({
        ...state,
        userInfo: action.payload,
        isAuthenticated: true,
      });
    },
  },
  initialState
);
