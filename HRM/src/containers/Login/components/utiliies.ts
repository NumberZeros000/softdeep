import * as Yup from "yup";
import { actions } from "../index";

export const getLoginValidationSchema = () => {
  return Yup.object().shape({
    email: Yup.string().required("Username is Required!"),
    password: Yup.string().required("Password is Required!"),
  });
};

//handle api for form==================================================================================
export function FormHandleLogin(dispatch, data) {
  dispatch(actions.handleLogin({ dispatch, data }));
}
