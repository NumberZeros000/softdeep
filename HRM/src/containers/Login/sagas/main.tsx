import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { Login } from "../../../APIs/authencation";
import { takeAction } from "../../../services/forkActionSagas";
//Email Login
export function* HandleEmailLogin(action) {
  const { dispatch, data } = action.payload;
  try {
    console.log(action.payload);
    const res = yield call(Login, data);
    if (!res.success) yield dispatch(actions.handleLoginFail(res.message));
    yield dispatch(actions.handleLoginSuccess(res.data));
  } catch (err) {
    yield put(dispatch(actions.handleLoginFail(err)));
  }
}
export function* handleLogin() {
  yield takeAction(actions.handleLogin, HandleEmailLogin);
}
////////////////////////////////////////////////////////////

export default [handleLogin];
