import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import Login, { name as nameOfLogin } from "../containers/Login";

export const staticReducers = {
  [nameOfLogin]: Login,
};

export default (history) =>
  combineReducers({
    ...staticReducers,
    router: connectRouter(history),
  });
