import { createStore, applyMiddleware, compose } from "redux";
import { createLogger } from "redux-logger";
import { routerMiddleware, connectRouter } from "connected-react-router";
import { createBrowserHistory, createMemoryHistory } from "history";

import createSagaMiddleware from "redux-saga";
import reducers from "./reducers";
import sagas from "./sagas";

const sagaMiddleware = createSagaMiddleware();
export const history = createBrowserHistory();

const middleware = routerMiddleware(history);
const loggerMiddleware = createLogger({
  predicate: () => process.env.NODE_ENV === "development",
});
const middlewares = [sagaMiddleware, middleware, loggerMiddleware];

export default function configureStore(initialState: any) {
  const store = createStore(
    reducers(history),
    initialState,
    compose(applyMiddleware(...middlewares))
  );

  sagaMiddleware.run(sagas);
  return {
    store,
    history,
  };
}

// export const history = createBrowserHistory();

// export default function configureStore(preloadedState) {
//   const store = createStore(
//     reducers(history), // root reducer with router state
//     preloadedState,
//     compose(
//       applyMiddleware(
//         routerMiddleware(history) // for dispatching history actions
//         // ... other middlewares ...
//       )
//     )
//   );

//   return store;
// }
