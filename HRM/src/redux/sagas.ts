/**
 * @file init sagas
 */

import { all } from "redux-saga/effects";
import { sagas as Login } from "../containers/Login";

export const defaultSagaLists = [Login];

export default function* rootSaga() {
  yield all([Login()]);
}
